import os

from sslw.conf.production import *

from ..branding import *
from ..secrets import *

#Set email to local
EMAIL_HOST = '127.0.0.1'


INSTALLED_APPS.insert(0, 'sslw.brand')
if os.path.exists(os.path.join(os.path.dirname(__file__), '..', '..','static')):
    STATICFILES_DIRS.append(os.path.join(os.path.dirname(__file__), '..', '..','static'))

#Enable Pondresvault
#INSTALLED_APPS.append('sslw.integrations.pondresvault')
	
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

SECRET_KEY = "i`&fRry}D4qj8]5n.OM;156[5=LD{f?{Q/RF<Kac^bDEECQE<5u]CWpD`YCy&@9^_]P<nvx1rs+e4*?>ZoIO?.KXs{Vf^,^#>I!g!{uhGDE7E=y_ftkn/|oW~;BB8@&/X=6Dn7CYz7Y:>N^=eFZsLoucz_6PoRsk[~&6PHgOc9g+t2tCXb)Z;!JBeOGLrChI:_]3V(}p:%eff%3ozwYRKfKMU$}cE$N*hyVN~tph/U/(NZ:);T.q!S<hX=k;H7J4"

ALLOWED_HOSTS = [
    '*',
    'dev.securedd.nl']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(SSLW_DIRECTORY, 'development.db'),
        'TEST_NAME': ':memory: '}}

NEXMO_API_KEY = 'fc44e551'
NEXMO_API_SECRET = '9JY7R3Fv'
NEXMO_API_URL = 'https://rest.nexmo.com/sms/json'

broker_host = 'localhost'  # CHANGEME
broker_user = 'demosslposteurope'
broker_vhost = 'demosslposteurope/sslw'
broker_password = 'OkkMBZagZZwNJXfM9rFBXgqK03AFsOtpdSZNslede7x1gBfdVjh5IPhNGLqVGV1r'
BROKER_URL = 'amqp://{}:{}@{}/{}'.format(broker_user, broker_password, broker_host, broker_vhost)

#Disable VERP - bounce handler
USE_VERP = False
USE_VERP_POLLER = False
VERP_ADDRESS = 'mv1+{{ message.connector }}.demo.sslposteurope.com@testses.sslpost.com'
# VERP_HOST = 'zimbra.protectedservice.net'
# VERP_USER  = 'demosslposteuropemessage@sslpost.com'
# VERP_ADDRESS = 'demosslposteuropemessage+{{ message.connector }}@sslpost.com'
# VERP_PASSWORD = 'Smw5wp7P0WxGpWFPHElcM1bMJmspWYLQYI4AGWm04LR7Xfhn5SVyRQHqzfyKcsjJ'
# VERP_SSL = True

USE_DKIM = False
DKIM_DOMAIN = 'sslposteurope.com'
DKIM_PRIVATE_KEY = """
-----BEGIN RSA PRIVATE KEY-----
MIICXgIBAAKBgQC5CD9AIcn3UwQTuvXD8x3qYcRNgXSCzk3/2ZYI744NB+Yi4/c8
+Rfscls/MKrr9kGGz4UoieeA+MmJMrQ6dZzDFs3OGbn7T+QJopgNxEp0xvecganK
5Jkr+2DPmCz/pOv0G2OUOAG3FIQGEj3az03IcSVxoQWJRG1PkWH5gIsz5QIDAQAB
AoGBALbjqfEv9Ltb+OmvoICga29iV2/sh0dLt9kuLpiCgDvtFNJj1dsUs0ro9bwN
1MQK0lX4BLw/Iu5d+oZ78Zze56RSlVRfXVVPJpr3/Jee/5CUYz6ljIlh6Ryhx7rQ
dp1waD9Sch1dddNX2hMOfNIJL01M/7sRpIotbIITbPH5vb7BAkEAu9D1hctpb430
bFCdHUM9fsJUE/2w5lZQkz2K9qoT/hIqOS3GvislWyzxTRBIu7WxnDbMHElsMX7T
cYqtP2JkCQJBAPw0jI4jto51vNH22m6lFSMnv1Dlt6im4QXf9Lq8LUgntuNFgitI
yRumwcbgugXeG6rzT0HywQp2ugczUVF3X/0CQD2876heeDcyW6FrUmo3GzTa3lZ/
8zGUOAJenZp9x/qhr/2Bva3vcuUovd1OOAp0LIEkRi4aOJWAkjLlmhOW3TkCQQCU
CTRHamcJj8pZZBAqyfNyQy5lCbGX6PxEOoSn5zSdGdKb7HzPc1W4SVWMqvQF5a1P
1qk7CIaO4cmn2FNvAibxAkEAtA8KM6yF4x1AHy5Sbicm6lrG7aLC9rfhQFg5HpVy
JNm3TO9uGTr/f6OOhhFqD3ukLaD6awHYb/Qrllv9CZFUlg==
-----END RSA PRIVATE KEY-----
""".strip()

# Multitenant
LOGGING['handlers']['file']['filename'] = '/data-node/demosslposteurope/log/sslw.log'
CACHES['default']['LOCATION'] = '/data-node/demosslposteurope/cache/default'
CACHES['decoded']['LOCATION'] = '/data-node/demosslposteurope/cache/decoded'
STATIC_ROOT = '/data-node/demosslposteurope/static'
SESSION_FILE_PATH = '/data-node/demosslposteurope/session'
EMAIL_FILE_PATH = '/data-node/demosslposteurope/tmp/email'
MEDIA_ROOT = '/data-cluster/demosslposteurope/media'
# STORE_LOCAL = '/data-cluster/demosslposteurope/store'
STORE_STORAGE = {
    'storage': 'django.core.files.storage.FileSystemStorage',
    'location': '/data-cluster/demosslposteurope/store'}
ENCRYPTED_STORE_UPLOAD_TEMP_DIR = '/data-cluster/demosslposteurope/upload'
JUMPLOADER_UPLOAD_TEMP_DIR = '/data-cluster/demosslposteurope/uploader'

CAPTCHA_PRIVATE_KEY = None  # CHANGEME
CAPTCHA_PUBLIC_KEY = None  # CHANGEME

WEB_ROOT = 'https://dev.securedd.nl'
MEDIA_URL = '/static/'

EMAIL_FROM_SYSTEM = 'noreply@securedd.nl'
EMAIL_SENDER_DOMAIN = 'securedd.nl'

GOOGLE_ANALYTICS_TRACKING_ID = None

#USE_STORAGE_REPLICATOR = True
#USE_STORAGE_REPLICATOR_REPLOG = True

#STORAGE_REPLICATOR_REPLICAS = {
#    's3-demosslposteurope-store': {
#         'storage': 'sslw.storage.backends.S3BotoStorageWithRegion',
#         'region': 'eu-west-1',
#         'calling_format': 'boto.s3.connection.OrdinaryCallingFormat',
#         'bucket_name': 'demosslposteurope-store',
#         'access_key': None,  # CHANGEME
#         'secret_key': None,  # CHANGEME
#         'default_acl': 'private',
#         'encryption': True,
#         'gzip': False}}
#
#STATICFILES_STORAGE = 'sslw.storage.backends.S3BotoStorageWithRegion'
#AWS_QUERYSTRING_AUTH = False
#AWS_ACCESS_KEY_ID = None  # CHANGEME
#AWS_SECRET_ACCESS_KEY = None  # CHANGEME
#AWS_STORAGE_BUCKET_NAME = 'demosslposteurope-static.s5tstatic.net'
#STATIC_URL = 'https://demosslposteurope-static.s5tstatic.net/'
#AWS_S3_CUSTOM_DOMAIN = 'demosslposteurope-static.s5tstatic.net'
#AWS_S3_CALLING_FORMAT = 'boto.s3.connection.OrdinaryCallingFormat'
#AWS_S3_REGION = 'eu-west-1'

CROSSDOMAIN_ALLOW_ACCESS_FROM = [
    'demo.sslposteurope.com']

#RESOURCE_STORAGE = {
#    'storage': 'sslw.storage.backends.S3BotoStorageWithRegion',
#    'region': 'eu-west-1',
#    'bucket_name': 'demosslposteurope-media.s5tstatic.net',
#    'custom_domain': 'demosslposteurope-media.s5tstatic.net',
#    'calling_format': 'boto.s3.connection.OrdinaryCallingFormat',
#    'access_key': None,  # CHANGEME
#    'secret_key': None,  # CHANGEME
#    'default_acl': 'public-read',
#    'encryption': False,
#    'gzip': False}
