DOMAIN = 'dev.securedd.nl'
BRAND_ALIAS = 'securedd'
BRAND_ID = 33350

BRAND = "SECUREDD"

from sslw.conf.production import INSTALLED_APPS
if not 'sslw.integrations.pondresvault' in INSTALLED_APPS:
    if 'sslw.integrations.saml' in INSTALLED_APPS:
        INSTALLED_APPS.remove('sslw.integrations.saml')
    INSTALLED_APPS.append('sslw.integrations.pondresvault')
    INSTALLED_APPS.append('sslw.integrations.saml')

AUTH_PREFER_ALIAS = True
#Enable Deaddrop

INSTALLED_APPS.append('sslw.integrations.deaddrop.apps.DeaddropConfig')
DEADDROP_CREATE_OPTIONS_ON_USER_CREATED = True
DEADDROP_SET_NEW_CASES_AS_CASE = True
DEADDROP_ENFORCE_CASE_FUNCTIONS = True

COUNTRY_CODE = 'NL'
LANGUAGE_CODE = 'nl'
TIME_ZONE = TIMEZONE = 'Europe/Amsterdam'
LANGUAGES = [
    ('nl', "Dutch")]
POWERED_BY = "SECUREDD"

EMAIL_FROM_SYSTEM = 'info@securedd.nl'

SUPPORT = [('SECUREDD support', 'support@securedd.nl')]
SUPPORT_EMAIL = 'support@securedd.nl'
SUPPORT_TELEPHONE = '+31 (0) 182 79 10 10'

TERMS_AND_CONDITIONS_URL = 'https://www.securedd.nl/terms-conditions/'
PRIVACY_POLICY_URL = 'https://www.securedd.nl/privacy-policy/'
HELP_URL = None

USE_SIGNUP = False

AUDITING_VISIT = False
AUDITING_VISIT_PING = False

SHOW_COMMIT = False
SHOW_NODE = False
SMS_FROM = "SECUREDD"
SMS_FALLBACK_FROM = {
    1: '12015109275'}
SMS_DEFAULT_COUNTRY_CODE = 44

###################################
###Activation pondresvault style###
###################################
LOGIN_FORM = 'sslw.integrations.pondresvault.forms.LoginFromWithConfirmationValidation'
ALIAS_EMAIL_CONFIRMATION_VIEW = 'pondresvault:alias-confirm'
ALIAS_EMAIL_CONFIRMATION_SUBJECT = "Activatie van uw SECUREDD eSafe"

DEADDROP_CASE_IDENTIFIER = 'sdd'

USE_NAME_FOR_LOGGED_IN_AS = True


USE_SAML_ACCOUNT_ACTIVATION = True
SAML_ACCOUNT_ACTIVATION_VIEW = 'pondresvault:account-activate'

PONDRES_VAULT_CREATE_PREFERENCES_ON_USER_CREATED = True
PONDRES_VAULT_POPULATE_REGISTRAR_RESULT_FROM_PREFERENCES = True

#PORTAL_BASE_DOMAIN = 'securedd.nl'
ADMIN_USER_FORM = 'sslw.integrations.pondresvault.forms.AdminUserPreferencesForm'

REGISTRAR_DEFAULT_REGISTRAR_NAME = 'esafe'
REGISTRAR_REGISTER_ACCOUNT_ACTIVATE_URL = '/pondresvault/account/activate/'
#REGISTRAR_LOGIN_FORM = 'sslw.integrations.pondresvault.forms.RegistrarLoginFormWithDateOfBirthPrepended'
PONDRES_VAULT_ACTIVATE_FOCUS_ON_PASSWORD = False  # TODO: Remove after Digi migration
REGISTRAR_REGISTER_FORM = 'sslw.integrations.pondresvault.forms.RegisterFormWithDateOfBirth'
REGISTRAR_FORGOTTEN_FORM = 'sslw.integrations.pondresvault.forms.RegistrarForgottenPasswordFormWithDateOfBirth'

PONDRES_VAULT_ACTIVATE_SET_PASSWORD = True
PONDRES_VAULT_ACTIVATE_VIEW_PARTS = [
    'sslw.integrations.pondresvault.parts.AliasWithConfirmation',
    'sslw.integrations.pondresvault.parts.SetPasswordIfEnabled',
    'sslw.app.parts.account.TermsAndConditions',
    'sslw.app.parts.account.PrivacyPolicy']

PONDRES_VAULT_SETTINGS_PARTS = [
    'sslw.integrations.pondresvault.parts.PreferencesSummary',
    'sslw.integrations.pondresvault.parts.AliasSummary']

#################################
###End activation pondresvault###
#################################

from sslw.conf.production import ADMINS
ADMINS.append(('SECUREDD Errors', 'errors@securedd.nl'))

ADDITIONAL_LESS = """
/* Standard Font */

@standardFont: Arial, Helvetica, sans-serif;
@standardFontColour: #000000;

/* Standard Colours */

@primaryColour: #BFBFBF; /* Main button and header colours */
@secondaryColour: #FF6600;
@tertiaryColour: #005AA5;
@buttonColour: #000000;

/* Page Background */

@pageBackgroundColour: #EFEFEF; /* Background surrounding the mainPage */

/* Header Elements */

@pageHeaderLink: #196836;
@pageHeaderText: #000000;

/* Login, Decrypt Pages */

@loginPageHeaderColour: #FFFFFF;
@loginPageContentColour: #FFFFFF;
@loginBoxColour: #E9EAE8;
@loginTitleColour: #000000;
@loginFooterColour: #333333;

/* Main Pages */

@mainPageColour: #FFFFFF; /* background of main container of content */
@mainContentColour: #FFFFFF; /* background of sub containers with content */
@mainFooterColour: #333333;

/* Titles to Black instead of light grey */
.HeaderTitle { color: #000; }
.subtitleHighlight { color: #000;}
.titleHighlight { color: #000;}

/* Highlights to house-style orange */
.prmHighlight { color: #EB6012; }
.headMenuLink, .headMenuLinkLogout { color:#EB6012; font-weight: bold;}

/* Links to Blue for clarity, hover color to Purple, with links standard underlined */
.helptext a { color: #0000EE;}
.helptext a:hover { color:#800080;}
.stdLink { color: #000; text-decoration: underline;}

/* Customize submenu item color */
.menuItem { font-style: italic; }

/* Make login title black */
.sslLoginTitle { color: #000 }

/* Set borders to circular * { border-radius: 0px 50px 30px 0px; } */
* { border-radius: 0px; }

/* Change color of the submenu highlights */
.accordion ul a:hover { background: #FFF; }
.accordion ul a.active{ background: #FFF !important; }

#headerAdmin {
  display: none;
}

#headerSettings {
  display: none;
}

"""
