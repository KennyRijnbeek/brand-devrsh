{% if user.pondres_preferences.last_name %}
    {% if user.pondres_preferences.gender == 'f' %}
        Geachte mevrouw {{ user.pondres_preferences.last_name }},
    {% elif user.pondres_preferences.gender == 'm' %}
        Geachte heer {{ user.pondres_preferences.last_name }},
    {% else %}
        Geachte heer, mevrouw {{ user.pondres_preferences.last_name }},
    {% endif %}
{% endif %}

Bedankt voor het doorgeven van uw e-mailadres. Uw e-mailadres is vanaf nu uw gebruikersnaam voor het inloggen.

Bevestig uw gebruikersnaam door op de activatielink hieronder te klikken.

{{ url }}

Werkt de link niet? 
Als de link niet werkt, dan kunt u de link kopiëren en direct in de adresbalk van uw internetbrowser plaatsen. 

Heeft u vragen? 
Neem contact op met SECUREDD: support@securedd.nl

Met vriendelijke groet,
SECUREDD
